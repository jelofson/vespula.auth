# README #

A simple, flexible authentication class that is easy to set up and understand.

There are currently 3 adapters:

* File - using a .htaccess or array of users and encrypted passwords
* Sql - using a database table
* Ldap - Authenticating against an LDAP/AD server
* Multi - Authenticating against more than one adapter

## Documentation

Documentation is available at
https://vespula.bitbucket.io/auth/
