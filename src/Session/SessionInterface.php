<?php
namespace Vespula\Auth\Session;

/**
 * Session Interface 
 * 
 * @author Jon Elofson <jon.elofson@gmail.com>
 *
 */
interface SessionInterface 
{

    /**
     * Get the current session status
     * 
     */
    public function getStatus(): ?string;
	
    /**
     * Set the status for the session
     * 
     */
    public function setStatus(string $status): void;
    
    /**
     * Set the username in session store
     * 
     */
    public function setUsername(string $username): void;
    
    /**
     * Get the username from session store
     * 
     * @return string
     */
    public function getUsername(): ?string;
    
    /**
     * Get extra userdata from the session store 
     * 
     * @see \Vespula\Auth\Adapter\AdapterInterface::lookupUserData()
     * @return array
     */
    public function getUserdata():?array;
    
    /**
     * Set the userdata in session store
     * 
     * @param mixed[] $userdata
     */
    public function setUserdata(array $userdata): void;
    
    /**
     * Check to see if the session has gone idle based on idle time
     * 
     */
    public function isIdled(): bool;
    
    /**
     * Check to see if the session has expired based on expire time
     *
     */
    public function isExpired(): bool;
    
    /**
     * Clear certain session data (userdata, username)
     *
     */
    public function reset(): void;

    /**
     * Regenerate the session id.
     */
    public function regenerateId(): void;
}
