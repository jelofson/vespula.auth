<?php
namespace Vespula\Auth\Session;

use BadMethodCallException;
use InvalidArgumentException;
use Vespula\Auth\Exception;
/**
 * Session class for managing user data and auth status 
 * 
 * @author @author Jon Elofson <jon.elofson@gmail.com>
 *
 */
class Session implements SessionInterface 
{
    /**
     * https://www.php.net/manual/en/session.configuration.php#ini.session.gc-maxlifetime
     */
    public const DEFAULT_SESSION_GC_MAXLIFETIME = 1440; // (24 minutes)
    
    /**
     * https://www.php.net/manual/en/session.configuration.php#ini.session.cookie-lifetime
     */
    public const DEFAULT_SESSION_COOKIE_LIFETIME = 0;
                                                       


    /**
     * The session segment (array key) used to segregate this session data
     * 
     */
    protected string $key;

    /**
     * The max idle time allowed. 0 for no check. Default is session.gc_maxlifetime
     * 
     */
    protected int $idle;
    
    /**
     * The time allowed before the session expires. Default is session.cookie_lifetime.
     * 0 means no check
     * 
     */
    protected int $expire;
    
    /**
     * The friendly reference to the session data
     * 
     * @var array
     */
    protected $store = [];
    
    /**
     * Constructor
     * 
     * @param integer|null $idle Set an idle max time allowed. 0 for no max.
     * @param integer|null $expire Set the time when session expires. 0 for no expire.
     * @param string|null $key a unique key for the session entry within $_SESSION that 
     * contains this class' session data. Defaults to __CLASS__
     */
    public function __construct(?int $idle = null, ?int $expire = null, ?string $key = null)
    {
        if (session_status() !== PHP_SESSION_ACTIVE) { 
            session_start(); 
        }
        $this->key = 
            (!is_null($key) && mb_strlen($key, 'UTF-8') > 0)
            ? $key : self::class ;
        
        if (! isset($_SESSION[$this->key])) {
            $_SESSION[$this->key] = [
                'status'=>null,
                'timestamp'=>time(),
                'username'=>null,
                'userdata'=>[],
                'interval'=>0
            ];
        }
        
        /** @psalm-suppress UnsupportedPropertyReferenceUsage */
        $this->store =& $_SESSION[$this->key];

        $this->setIdle($idle);
        $this->setExpire($expire);
        $this->updateInterval();
  
    }

    /**
     * Update the time since last request. Used to determine idle/expired
     * 
     */
    protected function updateInterval(): void
    {
        $now = time();
        if (! array_key_exists('timestamp', $this->store)) {
            $this->store['timestamp'] = $now;
        }
        $prev = $this->store['timestamp'];
        $this->store['interval'] = $now - $prev;
        $this->store['timestamp'] = $now;
    }
    
    /**
     * Set the idle time. Check that it is not greater than session.gc_maxlifetime.
     * 
     * @throws \Exception
     */
    public function setIdle(?int $idle): void
    {
        $defaulIniIdleVal = ini_get('session.gc_maxlifetime'); // may be string|false
        // this->idle is declared as an int property, make sure we cast to int
        $this->idle = ($defaulIniIdleVal === false) 
                        ? static::DEFAULT_SESSION_GC_MAXLIFETIME 
                        : ((int)$defaulIniIdleVal); 
        if ($idle !== null) {
            
            if ($idle > $defaulIniIdleVal) {
                throw new Exception('Idle time greater than gc_maxlifetime');
            }
            $this->idle = $idle;
        }
    }
    
    public function getIdle(): int
    {
        return $this->idle;
    }
    
    /**
     * Set the max time before expiry. Must not be greater than session.cookie_lifetime
     * 
     * @throws \Exception
     */
    public function setExpire(?int $expire): void
    {
        $currentCookieLifetime = ini_get('session.cookie_lifetime');
        $this->expire  = ($currentCookieLifetime === false)
                         ? static::DEFAULT_SESSION_COOKIE_LIFETIME
                         : ((int)$currentCookieLifetime);
        if ($expire !== null) {
            
            $cookie_lifetime = $currentCookieLifetime;
            if ($cookie_lifetime > 0 && $expire > $cookie_lifetime) {
                throw new Exception('Expire time greater than cookie_lifetime');
            }
            $this->expire = $expire;
        }
    }
    
    public function getExpire(): int
    {
        return $this->expire;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see \Vespula\Auth\Session\SessionInterface::isIdled()
     */
    public function isIdled(): bool
    {
        return $this->idle > 0 && $this->store['interval'] >= $this->idle;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see \Vespula\Auth\Session\SessionInterface::isExpired()
     */
    public function isExpired(): bool
    {
        return $this->expire > 0 && $this->store['interval'] >= $this->expire;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see \Vespula\Auth\Session\SessionInterface::reset()
     */
    public function reset(): void
    {
        $this->store['username'] = null;
        $this->store['userdata'] = null;
    }
    
    
    /**
     * {@inheritDoc}
     * 
     * @see \Vespula\Auth\Session\SessionInterface::setStatus()
     */
    public function setStatus(string $status): void
    {
        $this->store['status'] = $status;
    }
    
    /**
     * {@inheritDoc}
     *
     * @see \Vespula\Auth\Session\SessionInterface::getStatus()
     */
    public function getStatus(): ?string
    {
        return $this->store['status'];
    }
    
    /**
     * {@inheritDoc}
     *
     * @see \Vespula\Auth\Session\SessionInterface::setUsername()
     */
    public function setUsername(string $username): void
    {
        $this->store['username'] = $username;
    }
    
    /**
     * {@inheritDoc}
     *
     * @see \Vespula\Auth\Session\SessionInterface::getUsername()
     */
    public function getUsername(): ?string
    {
        return $this->store['username'];
    }
    
    /**
     * {@inheritDoc}
     *
     * @see \Vespula\Auth\Session\SessionInterface::setUserdata()
     */
    public function setUserdata(array $userdata): void
    {
        $this->store['userdata'] = $userdata;
        
    }
    
    /**
     * {@inheritDoc}
     *
     * @see \Vespula\Auth\Session\SessionInterface::getUserdata()
     */
    public function getUserdata(): ?array
    {
        return $this->store['userdata'];
    }
    
    /**
     * Regenerate the session id.
     */
    public function regenerateId(): void
    {
        session_regenerate_id(true);
    }

    public function __call($method, $args)
    {
        if (substr($method, 0, 3) == 'get') {
            $key = lcfirst(substr($method, 3));
            if (isset($this->store[$key])) {
                return $this->store[$key];
            }
            // Note that this does not SET the value in the store. Just returns a default if it's not there
            return $args[0] ?? null;
        }

        if (substr($method, 0, 3) == 'set') {
            $key = lcfirst(substr($method, 3));
            $this->store[$key] = $args[0] ?? null;
            return;
        }

        throw new BadMethodCallException('Method does not exist');
    }
    
}