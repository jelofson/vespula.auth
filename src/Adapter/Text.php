<?php
namespace Vespula\Auth\Adapter;

use \Vespula\Auth\CryptoUtils;

/**
 * This class is for authenticating users by simple text data. This is
 * for testing purposes only and should not be used in production.
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 *
 */
class Text extends AbstractAdapter
{

    /**
     * Error when no user found in the passwords array
     *
     * @var string
     */
    public const ERROR_NO_USER = 'ERROR_NO_USER';


   /**
    *
    * @see setPassword()
    */
    protected array $passwords = []; // Hashed passwords keyed on username

    /**
     *
     * @see setUserData()
     */
    protected array $userdata = []; // array of userdata


    /**
     * 
     * @param array|string $data
     * 
     * @throws \Vespula\Auth\Exception
     * 
     * @psalm-suppress RedundantConditionGivenDocblockType
     */
    public function __construct($data)
    {
        if (is_string($data)) {
            $this->passwords = $this->loadFromFile($data);
        } else if (is_array($data)) {
            $this->passwords = $data;
        } else {
            throw new \Vespula\Auth\Exception('Invalid data passed. Must be a filename or array of users');
        }
    }

    /**
     * @return array<string, string>
     */
    protected function loadFromFile(string $filename): array
    {
        if (! file_exists($filename)) {
            throw new \Vespula\Auth\Exception('File not found ' . $filename);
        }
        $lines = file($filename, FILE_IGNORE_NEW_LINES);

        $passwords = [];
        foreach ($lines as $line) {
            $data = explode(":", $line);
            $passwords[$data[0]] = $data[1];
        }

        return $passwords;

    }

    /**
     * {@inheritDoc}
     * @see \Vespula\Auth\Adapter\AdapterInterface::authenticate()
     */
    public function authenticate(array $credentials): bool
    {
        // explicit vs `extract`
        $username = $credentials['username'];
        $password = $credentials['password'];

        if (! isset($this->passwords[$username])) {
            $this->error = Text::ERROR_NO_USER;
            return false;
        }

        if (substr($this->passwords[$username], 0, 4) == '$apr') {
            return $this->apr1md5(''.$password, ''.$this->passwords[$username]);
        }

        if (substr($this->passwords[$username], 0, 4) == '$2y$') {
            return $this->php_verify(''.$password, $this->passwords[$username]);
        }

        return false;
    }

    protected function apr1md5(string $password, string $hash): bool
    {
        return CryptoUtils::check($password, $hash);
    }

    protected function php_verify(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }

    /**
     * Set user-specific data
     *
     */
    public function setUserData(string $username, array $data):void
    {
        $this->userdata[$username] = $data;
    }
    
    /**
     * Loads user data from an array. Array must have keys for users and an array 
     * of data for each user.
     * 
     * $data = [
     *     'juser'=>[
     *         'fullname'=>'Joe User',
     *         'email'=>'juser@awesome.com'
     *     ],
     * ];
     * 
     * @param array $data user data
     */
    public function loadUserData(array $data): void
    {
        foreach ($data as $username=>$userdata) {
            $this->setUserData($username, $userdata);
        }
    }

    /**
     *
     * {@inheritDoc}
     * @see \Vespula\Auth\Adapter\AdapterInterface::lookupUserData()
     */
    public function lookupUserData(string $username): array
    {
        return array_key_exists($username, $this->userdata) ? $this->userdata[$username] : [];
    }

    /**
     *
     * {@inheritDoc}
     * @see \Vespula\Auth\Adapter\AdapterInterface::getError()
     */
    public function getError(): string
    {
        return $this->error;
    }
}