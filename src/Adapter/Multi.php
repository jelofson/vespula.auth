<?php

namespace Vespula\Auth\Adapter;

/**
 * This class is for authenticating users against multiple adapters. 
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 *
 */
class Multi extends AbstractAdapter
{
    /**
     * The array of adapters to authenticate against
     * 
     * @var array 
     */
    protected $adapters = [];
    
    /**
     * The adapter used for a give user. Set after successful authentication
     * @var array
     */
    protected $userAdapters = [];
    
    /**
     * Add an authentication adapter
     * 
     * @param \Vespula\Auth\Adapter\AdapterInterface $adapter an authentication adapter
     */
    public function addAdapter(AdapterInterface $adapter): void
    {
        $this->adapters[] = $adapter;
    }
    
    /**
     * Try to authenticate the user against any of the defined adapters. As soon
     * as one authenticates, it quits.
     * 
     */
    public function authenticate(array $credentials): bool
    {
        foreach ($this->adapters as $adapter) {
            $valid = $adapter->authenticate($credentials);
            if ($valid) {
                $this->userAdapters[$credentials['username']] = $adapter;
                return true;
            }
        }
        return false;
    }
    
    
    /**
     * You can set an adapter for a given user, however, if authentication occurs
     * and is successful, the user's adapter will match whichever authenticated it.
     * 
     */
    public function setUserAdapter(string $username, AdapterInterface $adapter): void
    {
        $this->userAdapters[$username] = $adapter;
    }
    
    /**
     * Get the adapter that the user logged in with (or false)
     * 
     * @return boolean|AdapterInterface
     */
    public function getUserAdapter(string $username)
    {
        if (array_key_exists($username, $this->userAdapters)) {
            return $this->userAdapters[$username];
        }
        return false;
    }
    
    /**
     * Find userdata in from the adapter that authenticated the user.
     * 
     * @return mixed[]
     * 
     */
    public function lookupUserData(string $username):array
    {
        $info = [];
        $userAdapter = $this->getUserAdapter($username);
        if ($userAdapter instanceof AdapterInterface) {
            return $userAdapter->lookupUserData($username);
        }
        
        return $info;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vespula\Auth\Adapter\AdapterInterface::getError()
     * 
     * A better approach for the multi adapter is to call getUserAdapter($username)
     * and then getError() on that adapter.
     * 
     * $userAdapter = $this->getUserAdapter('juser');
     * echo $userAdapter->getError();
     */
    public function getError(): string
    {

        foreach ($this->adapters as $adapter) {
            if ($adapter->getError()) {
                $this->error = $adapter->getError();
            }
        }
        return $this->error;
    }
}