<?php

namespace Vespula\Auth\Adapter;

abstract class AbstractAdapter implements AdapterInterface
{
    /**
     * Debugging error
     *
     */
    protected string $error = '';
}
