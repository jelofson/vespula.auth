<?php
namespace Vespula\Auth\Adapter;
use Vespula\Auth\Exception;

/**
 * This class is for authenticating users against Active Directory using LDAP.
 *
 * Please note the suppressed warning on the ultimate ldap_bind in authenticate()
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
class Ldap extends AbstractAdapter 
{
    /**
     * Debugging info
     *
     * @var string
     */
    public const ERROR_NO_DN = 'ERROR_NO_DN';

    /**
     * An LDAP connection
     *
     * @var resource
     * @psalm-suppress PropertyNotSetInConstructor
     */
    protected $conn;

    /**
     * The ldap server uri
     *
     */
    protected string $uri;

    /**
     * Port to connect to
     *
     */
    protected int $port;

    /**
     * The dn format, if known, for authenticating. If null, $bind_options must be set
     *
     * Format like this 'cn=%s, OU=City,OU=Country'
     *
     */
    protected ?string $dn;

    /**
     * An array of bind options for finding a user's DN
     *
     * Keys are:
     *
     * `basedn`: The base dn to search through
     * `binddn`: The dn used to bind to
     * `bindpw`: A password used to bind to the server using the binddn
     * `filter`: A filter used to search for the user. Eg. samaccountname=%s
     *
     */
    protected ?array $bind_options;

    /**
     * LDAP options you want set after connecting. As an array with $key and $value
     *
     * <code>
     * $ldap_options = [
     *     LDAP_OPT_PROTOCOL_VERSION=>3
     * ];
     * </code>
     *
     */
    protected ?array $ldap_options;

    /**
     * Extra attributes from the LDAP entry you want placed in the userdata array
     *
     * Note: this doesn't support aliases yet. You can't say ['internet_address'=>'email']
     *
     */
    protected ?array $attributes;

    /**
     * Characters to escape in username
     *
     */
    protected string $escapeChars = '\\&!|=<>,+-"\';()';


    /**
     * Constructor
     *
     * @param string $uri ldap.mycompany.org
     * @param string|null $dn the dn
     * @param mixed[]|null $bind_options Optional. Required if no $dn
     * @param mixed[]|null $ldap_options Optional LDAP options
     * @param mixed[] $attributes Attributes to retrieve from AD and populate $userdata
     * @param integer $port The port number. Default 389
     * @throws Exception
     */
    public function __construct(string $uri, ?string $dn = null, ?array $bind_options = null, ?array $ldap_options = null, array $attributes = [], int $port = 389)
    {
        if (! extension_loaded('ldap')) {
            throw new Exception('LDAP extension not loaded');
        }

        $this->uri = $uri;
        $this->port = $port;
        $this->dn = $dn;
        $this->bind_options = $bind_options;
        $this->ldap_options = $ldap_options;
        $this->attributes = $attributes;

        $this->conn = $this->connect($this->uri, $this->port);

        $this->setLdapOptions($this->conn, ($this->ldap_options ?? []) );
    }


    /**
     *
     * {@inheritDoc}
     * @see \Vespula\Auth\Adapter\AdapterInterface::authenticate()
     */
    public function authenticate(array $credentials): bool
    {
        $username = $credentials['username'];
        $password = $credentials['password'];

        if (empty($username)) {
            return false;
        }
        if (empty($password)) {
            return false;
        }

        $username = addcslashes($username, $this->escapeChars);

        if ($this->dn) {
            $this->dn = sprintf($this->dn, $username);

        } else {
            $this->checkBindOptions($this->bind_options ?? []);
            $dn = $this->findDn($this->conn, $username, ($this->bind_options ?? []) );
            $this->dn = \is_bool($dn) ? null : $dn; // do this because $this->dn is declared as a nullable string
        }

        // No exception here, but set an error for debugging.
        // If a DN was not found, that means they did not supply one, or it was not found in Active Directory
        // Don't continue without a DN, but let's capture an error for debugging.

        if (! $this->dn) {
            $this->error = Ldap::ERROR_NO_DN;
            return false;
        }
        // Suppress the warning here so that even in dev environments, we don't see it.
        // I don't want a warning if they type their password in wrong.
        // All other ldap functions should output appropriate warnings if enabled.
        $bind = $this->bindQuietly($this->conn, $this->dn, $password);
        return $bind !== false;
    }

    /**
     * Username is not required because we use a dn to look it up.
     *
     * {@inheritDoc}
     * @see \Vespula\Auth\Adapter\AdapterInterface::lookupUserData()
     */
    public function lookupUserData(string $username): array
    {
        $username = addcslashes($username, $this->escapeChars);

        $userdata = [];
        
        $dn = $this->dn ?? '';

        if (! $dn) {
            $this->checkBindOptions($this->bind_options ?? []);
            $dn = $this->findDn($this->conn, $username, ($this->bind_options ?? []) );
        }

        $dn_parts = $this->explodeDn($dn, 0);
        $attributes = $this->attributes ? array_values($this->attributes) : [];
        $result = $this->search($this->conn, $dn, $dn_parts[0] ?? '', $attributes );
        
        if ($result) {
            /** @psalm-suppress PossiblyInvalidArgument */
            $entry = $this->firstEntry($this->conn, $result);
            if ($entry !== false) {
                $userdata = $this->parseUserAttribs($this->conn, $entry, $this->attributes ?? []);
            }
        }
        $this->unbind($this->conn);
        return $userdata;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Vespula\Auth\Adapter\AdapterInterface::getError()
     */
    public function getError(): string
    {
        return $this->error;
    }

    /**
     * Set escape chars
     *
     */
    public function setEscapeChars(string $chars): void
    {
        $this->escapeChars = $chars;
    }

    /**
     * Get escape chars
     *
     */
    public function getEscapeChars(): string
    {
        return $this->escapeChars;
    }

    /**
     * Parse returned entry and make the userdata more friendly
     *
     * @param resource $conn LDAP connection
     * @param resource $entry Entry identifier
     * @param array $attribs Attribs to collect
     * @return array<int|string, mixed>
     */
    protected function parseUserAttribs($conn, $entry, array $attribs): array
    {
        $userdata = [];
        $useAttribAlias = $this->hasStringKeys($attribs);
        
        foreach ($attribs as $alias => $attrib) {
            $vals = $this->getValues($conn, $entry, $attrib);
            $count = array_pop($vals);
            $key = $useAttribAlias ? $alias : $attrib;
            if ($count == 1) {
                $userdata[$key] = $vals[0];
            } else {
                $userdata[$key] = $vals;
            }
        }

        return $userdata;
    }

    /**
     * Ensure bind options are set properly
     *
     * @throws Exception
     */
    protected function checkBindOptions(array $bind_options): void
    {
        if (! array_key_exists('basedn', $bind_options)) {
            throw new Exception('Missing basedn in bind options');
        }
        if (! array_key_exists('binddn', $bind_options)) {
            throw new Exception('Missing binddn in bind options');
        }
        if (! array_key_exists('bindpw', $bind_options)) {
            throw new Exception('Missing bindpw in bind options');
        }
        if (! array_key_exists('filter', $bind_options)) {
            throw new Exception('Missing filter in bind options');
        }
    }

    /**
     * Find a user's dn using $bind_options. This is common in organizations
     * that have multiple DNs for groups of individuals. Search a base DN for the userid,
     * then return that user's fully-qualified DN that can be used to authenticate against.
     *
     * Returns false if no dn is found
     *
     * @param resource $conn LDAP connection
     * @param string $username username
     * @param array $bind_options options for binding
     * @throws Exception
     * @return boolean|string
     * 
     * @noRector \Rector\TypeDeclaration\Rector\FunctionLike\ReturnTypeDeclarationRector
     */
    protected function findDn($conn, string $username, array $bind_options)
    {
        $dn = false;

        // rather than use extract, make this explicit.
        $binddn = $bind_options['binddn'];
        $bindpw = $bind_options['bindpw'];
        $basedn = $bind_options['basedn'];
        $filter = $bind_options['filter'];

        $bind = $this->bind($conn, $binddn, $bindpw);
        if (! $bind) {
            throw new Exception('Could not bind to basedn');
        }

        $searchfilter = sprintf($filter, $username);

        $resource = $this->search($conn, $basedn, $searchfilter);

        if ( $resource === false || (is_array($resource) && count($resource) === 0) ) {
            throw new Exception('The LDAP DN search failed');
        }
        
        /** @psalm-suppress PossiblyFalseArgument */
        $first = $this->firstEntry($conn, (\is_array($resource) ? \array_shift($resource) : $resource) );
        if ($first !== false) {
            $dn = $this->getUserDn($conn, $first);
        }
        return $dn;
    }

    /**
     * Set ldap options after connecting.
     *
     * @param resource|\LDAP\Connection $conn LDAP connection
     * @param array $options Options
     */
    protected function setLdapOptions($conn, array $options): void
    {
        foreach ($options as $option=>$value) {
            ldap_set_option($conn, $option, $value);
        }
    }

    /**
     * The following methods are wrappers around native ldap functions.
     * These are here to make testing the class easier as you can mock
     * these methods via PHPUnit.
     *
     */

    /**
     * Connect to ldap server
     *
     * @return resource Link identifier
     * @throws Exception
     * 
     * @TODO ldap_connect(?string $uri = null, int $port = 389): LDAP\Connection|false is deprecated
     *       new signature is ldap_connect(?string $uri = null): LDAP\Connection|false 
     *       see https://www.php.net/manual/en/function.ldap-connect
     */
    protected function connect(string $uri, int $port)
    {
        $conn = ldap_connect($uri, $port);
        if (! $conn) {
            throw new Exception('EXCEPTION_LDAP_CONNECT_FAILED');
        }
        return $conn;
    }

    /**
     * Bind to an ldap server
     *
     * @param resource | \LDAP\Connection $conn Link identifier
     */
    protected function bind($conn, string $dn, string $password): bool
    {
        return ldap_bind($conn, $dn, $password);
    }

    /**
     * Bind to and ldap server but suppress warnings.
     * Allow this to happen because we don't care (ever) if a bind
     * failed, especially due to a bad username and password. Normally, we don't
     * want to suppress thses and use ini settings to do that,
     * but in this case, it's ok.
     *
     * @param resource|\LDAP\Connection $conn Link identifier
     */
    protected function bindQuietly($conn, string $dn, string $password): bool
    {
        return @ldap_bind($conn, $dn, $password);
    }

    /**
     * Search the base dn for a user matching the search filter. This should be
     * unique to the user and must match by the user's userid passed to
     * authenticate
     *
     * @param resource|\LDAP\Connection $conn Link identifier
     * @param string $basedn The base dn to search
     * @param string $searchfilter Eg. samaccountname=%s
     * @return resource|resource[]|false
     */
    protected function search($conn, string $basedn, string $searchfilter, array $attributes = [])
    {
        return ldap_search($conn, $basedn, $searchfilter, $attributes);
    }

    /**
     * Get values from the directory keyed by $attrib
     *
     * @see firstEntry()
     *
     * @param resource|\LDAP\Connection $conn Link identifier
     * @param resource|\LDAP\ResultEntry $entry An entry from the directory
     * @return array
     */
    protected function getValues($conn, $entry, string $attrib)
    {
        return ldap_get_values($conn, $entry, $attrib);
    }

    /**
     * Get the first entry from a search result
     *
     * @param resource|\LDAP\Connection $conn Link identifier
     * @param resource|\LDAP\Result $resource Result identifier
     * @return resource|false
     */
    protected function firstEntry($conn, $resource)
    {
        return ldap_first_entry($conn, $resource);
    }

    /**
     * Get the DN from a returned result
     *
     * @param resource|\LDAP\Connection $conn Link identifier
     * @param resource|\LDAP\ResultEntry $first The first entry returned
     * @return string
     */
    protected function getUserDn($conn, $first)
    {
        return ldap_get_dn($conn, $first);
    }

    /**
     * Get the DN parts
     *
     * @return array|false
     */
    protected function explodeDn(string $dn, int $with_attrib)
    {
        return ldap_explode_dn($dn, $with_attrib);
    }

    /**
     * Disconnect from the server
     *
     * @param resource|\LDAP\Connection $conn Link identifier
     */
    protected function unbind($conn): bool
    {
        return ldap_unbind($conn);
    }

   /**
    * Determine if array has a string key
    *
    * @param array $array
    * return boolean
    */
    protected function hasStringKeys(array $array): bool
    {
        return count(array_filter(array_keys($array), 'is_string')) > 0;
    }
}