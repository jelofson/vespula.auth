<?php
namespace Vespula\Auth\Adapter;

use Vespula\Auth\Exception;

/**
 * This class is for authenticating users against a database table. 
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 *
 */
class Sql extends AbstractAdapter 
{
    
    /**
     * Debugging info
     * 
     * @var string
     */
    public const ERROR_NO_ROWS = 'ERROR_NO_ROWS';
    public const ERROR_USERNAME_COL = 'ERROR_USERNAME_COL';
    public const ERROR_PASSWORD_COL = 'ERROR_PASSWORD_COL';
    
    /**
     * a \PDO object
     */
    protected \PDO $pdo;
    
    /**
     * Array of columns that must have 'username' and 'password' in them 
     * 
     * You can supply them in the form of 'realcolumn'=>'alias'. For example:
     * 
     * <code>
     * // select bcryptpass AS password, username, internet_address AS email
     * $cols = [
     *     'bcryptpass'=>'password',
     *     'username',
     *     'internet_address'=>'email'
     * ];
     * // In this example, the password col and username col requirements are met via an alias
     * //  on the bcryptpass col.
     * </code>
     */
    protected array $cols = [];
    
    /**
     * The name of the `username` column. Can be an alias. See fixCols()
     * 
     */
    protected string $usernameCol = 'username';
    
    /**
     * What table to use
     */
    protected string $from;
    
    /**
     * Any additional where conditions.
     */
    protected ?string $where;
    
    /**
     * Userdata to be collected and stored in session from the row. 
     * 
     * Note that username and password are NOT included here.
     * 
     * From the above example, 'email' would be the only key in this array
     */
    protected array $userdata = [];
    
    
    /**
     * Constructor 
     * @param array<int|string, string> $cols
     */
    public function __construct(\PDO $pdo, string $from, array $cols, ?string $where = null)
    {
        $this->pdo = $pdo;
        $this->from = $from;
        $this->cols = $cols;
        $this->where = $where;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vespula\Auth\Adapter\AdapterInterface::authenticate()
     * @todo Use binding for the additional $where
     */
    public function authenticate(array $credentials): bool
    {
        $username = $credentials['username'];
        $password = $credentials['password'];
        
        $cols = $this->fixCols();

        $where = $this->usernameCol . " = :username";
        if ($this->where) {
            $where .= " AND $this->where";
        }
        
        $query = "SELECT $cols FROM {$this->from} WHERE $where LIMIT 1";
        
        // If PDO::ATTR_ERRMODE is set to PDO::ERRMODE_EXCEPTION, a PDOException will be thrown
        $statement = $this->pdo->prepare($query);

        if ($statement === false) { // PDO::ATTR_ERRMODE is not set to PDO::ERRMODE_EXCEPTION,
           
            $errMsg = 'PDO Error: ' . $this->pdo->errorCode() . PHP_EOL . PHP_EOL
                     . \implode(PHP_EOL, $this->pdo->errorInfo());
            throw new Exception($errMsg); 
        }
        
        $row = false;
        if ($statement->execute([':username'=>$username])) {
            $row = $statement->fetch(\PDO::FETCH_ASSOC);
        }
        
        // Simple debugging info (could not find the user in the table based on the where clause)
        if (! $row) {
            $this->error = Sql::ERROR_NO_ROWS;
            return false;
        }
        $this->userdata = $row;
        return password_verify($password, ''.$row['password']);
  
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vespula\Auth\Adapter\AdapterInterface::lookupUserData()
     */
    public function lookupUserData(string $username): array
    {
        $info = [];
        $ignore = ['username', 'password'];
        foreach ($this->userdata as $key=>$val) {
            if (! in_array($key, $ignore)) {
                $info[$key] = $val;
            }
        }
        return $info;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Vespula\Auth\Adapter\AdapterInterface::getError()
     */
    public function getError(): string
    {
        return $this->error;
    }
    
    /**
     * Ensure the cols array contains required keys and set up aliases
     * 
     * @throws Exception
     */
    protected function fixCols(): string
    {
        $cols = [];

        if (! in_array('username', $this->cols)) {
            throw new Exception(Sql::ERROR_USERNAME_COL);
        }
        if (! in_array('password', $this->cols)) {
            throw new Exception(Sql::ERROR_PASSWORD_COL);
        }
        
        $username_key = array_search('username', $this->cols);
        if (is_string($username_key)) {
            $this->usernameCol = $username_key;
        }

        foreach ($this->cols as $key=>$col) {
            if (is_string($key)) {
                $cols[] = "$key AS $col"; 
            } else {
                $cols[] = "$col";
            }
        }
        return implode(", ", $cols);
        
    }
}
