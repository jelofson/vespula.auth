<?php
namespace Vespula\Auth\Adapter;

use PHPUnit\Framework\TestCase;

class LdapTest extends TestCase 
{
    protected $methods;
    protected $params_dn;
    protected $params_bind;
    protected $params_aliases;
    
    
    public function setUp(): void
    {
        $uri = 'ldap.mycompany.org';
        $dn = 'cn=%s,OU=MyCompany,OU=City,OU=Province';
        
        $ldap_options = [
            'LDAP_OPTION_A'=>3,
        ];
        $bind_options = [
                'basedn'=>'OU=MyCompany,OU=City,OU=Province',
                'binddn'=>'cn=special,OU=MyCompany,OU=City,OU=Province',
                'bindpw'=>'bindpass',
                'filter'=>'samaccountname=%s'
        ];
        
        $attributes = [
                'givenname',
                'mail'
        ];
        
        $attribAliases = [
            'firstname'=>'givenName',
            'email'=>'mail',
            'city'=>'city'
        ];
        
        $this->methods = [
                'connect',
                'bind',
                'bindQuietly',
                'unbind',
                'search',
                'getValues',
                'firstEntry',
                'getUserDn',
                'explodeDn',
                'setLdapOptions',
        ];
        
        $this->params_dn = [
                $uri,
                $dn,
                null, 
                $ldap_options,
                $attributes
        ];
        $this->params_bind = [
                $uri,
                null,
                $bind_options,
                $ldap_options,
                $attributes
        ];
        
        $this->params_aliases = [
                $uri,
                'cn=juser,OU=MyCompany,OU=City,OU=Province',
                null,
                null,
                $attribAliases
        ];
    }
    
    
    
    public function testEscapeChars()
    {
        $adapter = new \Vespula\Auth\Adapter\Ldap('some.url');
        
        $username = 'some-user';
        
        $expectedEscaped = addcslashes($username, $adapter->getEscapeChars());
        
        $this->assertEquals($expectedEscaped, 'some\-user');
        
        $adapter->setEscapeChars('\\+');
        
        $expectedEscaped = addcslashes($username, $adapter->getEscapeChars());
        
        $this->assertEquals($expectedEscaped, 'some-user');
        
    }
    
}