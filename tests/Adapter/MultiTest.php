<?php
namespace Vespula\Auth\Adapter;

use \PDO;
use PHPUnit\Framework\TestCase;
use PHPUnit\Util\Xml\Validator;

class MultiTest extends TestCase
{
    protected $adapter;
    protected $pdo;
    protected $cols;
    
    public function setUp(): void
    {
        $this->adapter = new Multi();
        $filename = __DIR__ . '/htpasswords.txt';

        $textAdapter = new Text($filename);
        
        $textAdapter->setUserData('juser', [
            'fullname'=>'Joe User',
            'email'=>'juser@example.com'
        ]);
        
        $this->adapter->addAdapter($textAdapter);
        
        $this->pdo = new PDO('sqlite::memory:');
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->createDatabase();
        $this->cols = [
            'userid'=>'username',
            'bcryptpass'=>'password',
            'fullname'=>'full_name',
            'email'
        ];
        $where = 'active=1';
        $sqlAdapter = new Sql($this->pdo, 'user', $this->cols, $where);
        
        $this->adapter->addAdapter($sqlAdapter);
        
    }
    
    public function createDatabase()
    {
        $sql = "CREATE TABLE user (
            userid VARCHAR(12) PRIMARY KEY, 
            fullname VARCHAR(50), 
            email VARCHAR(150), 
            bcryptpass VARCHAR(200),
            active INT
        )";
        
        $this->pdo->query($sql);
        
        $rows = [
            [
                'userid'=>'mclovin',
                'fullname'=>'McLovin',
                'email'=>'mclovin@awesome.com',
                'bcryptpass'=>password_hash('secret', PASSWORD_DEFAULT),
                'active'=>1
            ]
        ];

        
        $insert = "INSERT INTO user (userid, fullname, email, bcryptpass, active) "
                . "VALUES (:userid, :fullname, :email, :bcryptpass, :active)";
        
        $statement = $this->pdo->prepare($insert);
        
        foreach ($rows as $row) {
            $statement->execute($row);
        }
    }
    
    public function testAuthenticate()
    {
        $this->assertTrue($this->adapter->authenticate([
            'username'=>'juser',
            'password'=>'foobar'
        ]));
        
        $this->assertTrue($this->adapter->authenticate([
            'username'=>'mclovin',
            'password'=>'secret'
        ]));
    }
    
    public function getGetUserDataSql()
    {
        $this->adapter->authenticate([
            'username'=>'mclovin',
            'password'=>'secret'
        ]);
        
        $info = $this->adapter->lookupUserData('juser');
        $expected = [
            'full_name'=>'McLovin',
            'email'=>'mclovin@awesome.com'
        ];
        $this->assertEquals($expected, $info);
        
    }
    
    public function testGetUserDataText()
    {
        $filename = __DIR__ . '/htpasswords.txt';

        $textAdapter = new Text($filename);
        
        $this->adapter->addAdapter($textAdapter);
        
        $valid = $this->adapter->authenticate([
            'username'=>'juser',
            'password'=>'foobar'
        ]);
        
        $this->assertTrue($valid);
        
        $info = $this->adapter->lookupUserData('juser');

        $expected = [
            'fullname'=>'Joe User',
            'email'=>'juser@example.com'
        ];
        $this->assertEquals($expected, $info);
        
    }
    
    public function testGetUserAdapter()
    {

        $this->adapter->authenticate([
            'username'=>'juser',
            'password'=>'foobar'
        ]);
        
        $this->adapter->authenticate([
            'username'=>'mclovin',
            'password'=>'secret'
        ]);
        
        $txtadapter = $this->adapter->getUserAdapter('juser');
        $sqladapter = $this->adapter->getUserAdapter('mclovin');
        
        
        $this->assertEquals(Text::class, get_class($txtadapter));
        $this->assertEquals(Sql::class, get_class($sqladapter));
    }
}