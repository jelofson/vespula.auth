<?php
namespace Vespula\Auth;

use Vespula\Auth\CryptoUtils;
use PHPUnit\Framework\TestCase;

class CryptoUtilsTest extends TestCase 
{

    public function setUp(): void
    {
        
    }

    public function testHash_WhiteHat101() {
        $this->assertTrue(
            CryptoUtils::check('WhiteHat101','$apr1$HIcWIbgX$G9YqNkCVGlFAN63bClpoT/')
        );
    }

    public function testHash_apache() {
        $this->assertTrue(
            CryptoUtils::check('apache','$apr1$rOioh4Wh$bVD3DRwksETubcpEH90ww0')
        );
    }

    public function testHash_ChangeMe1() {
        $this->assertTrue(
            CryptoUtils::check('ChangeMe1','$apr1$PVWlTz/5$SNkIVyogockgH65nMLn.W1')
        );
    }
    
    public function testHash_WhiteHat101B() {
        $this->assertEquals(
            '$apr1$HIcWIbgX$G9YqNkCVGlFAN63bClpoT/',
            CryptoUtils::hash('WhiteHat101','HIcWIbgX')
        );
    }

    public function testHash_apacheB() {
        $this->assertEquals(
            '$apr1$rOioh4Wh$bVD3DRwksETubcpEH90ww0',
            CryptoUtils::hash('apache','rOioh4Wh')
        );
    }

    public function testHash_ChangeMe1B() {
        $this->assertEquals(
            '$apr1$PVWlTz/5$SNkIVyogockgH65nMLn.W1',
            CryptoUtils::hash('ChangeMe1','PVWlTz/5')
        );
    }

    // Test some awkward inputs

    public function testHash_ChangeMe1_blankSalt() {
        $this->assertEquals(
            '$apr1$$DbHa0iITto8vNFPlkQsBX1',
            CryptoUtils::hash('ChangeMe1','')
        );
    }

    public function testHash_ChangeMe1_longSalt() {
        $this->assertEquals(
            '$apr1$PVWlTz/5$SNkIVyogockgH65nMLn.W1',
            CryptoUtils::hash('ChangeMe1','PVWlTz/50123456789')
        );
    }

    public function testHash_ChangeMe1_nullSalt() {
        $hash = CryptoUtils::hash('ChangeMe1');
        $this->assertEquals(37, strlen($hash));
    }

    public function testHash__nullSalt() {
        $hash = CryptoUtils::hash('');
        $this->assertEquals(37, strlen($hash));
    }

    // a null password gets coerced into the blank string.
    // is this sensible?
    public function testHash_null_nullSalt() {
        $hash = CryptoUtils::hash(null);
        $this->assertEquals(37, strlen($hash));
    }
    
    public function testSaltType() {
        //$this->assertInternalType('string', CryptoUtils::salt());
        $this->assertIsString(CryptoUtils::salt());
    }

    public function testSaltPattern() {
        $this->assertMatchesRegularExpression('/.{8}/', CryptoUtils::salt());
    }

    public function testSaltRamdomness() {
        $this->assertNotEquals(CryptoUtils::salt(), CryptoUtils::salt());
    }
}
