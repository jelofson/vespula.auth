<?php
namespace Vespula\Auth\Session;

use BadMethodCallException;
use Vespula\Auth\Session\Session;
use Vespula\Auth\Auth;
use PHPUnit\Framework\TestCase;

class SessionTest extends TestCase 
{
    
    protected $session;

    public function __construct()
    {
        parent::__construct(null, [], '');
        
        $this->session = new Session();
        $_SESSION['Vespula\\Auth\\Session\\Session']['interval'] = 1200;
    }
    
    
    
    public function testInitial()
    {
        $this->assertNull($this->session->getStatus());
        $this->assertNull($this->session->getUsername());
        $this->assertEmpty($this->session->getUserdata());
    }
    
    public function testSetGetUsername()
    {
        $this->session->setUsername('juser');
        $this->assertEquals($this->session->getUsername(), 'juser');
    }
    
    public function testSetGetUserdata()
    {
        $userdata = [
            'fullname'=>'Joe User'
        ];
        $this->session->setUserdata($userdata);
        $this->assertEquals($this->session->getUserdata(), $userdata);
    }
    
    public function testReset()
    {
        $this->session->reset();
        $this->assertNull($this->session->getUsername());
        $this->assertNull($this->session->getUserdata());
    }
    
    public function testSetGetStatus()
    {
        $this->session->setStatus(Auth::ANON);
        $this->assertEquals(Auth::ANON, $this->session->getStatus());
    }
    
    public function testSetGetIdle()
    {
        $this->session->setIdle(1200);
        $this->assertEquals(1200, $this->session->getIdle());
    }
    
    public function testSetGetExpire()
    {
        $this->session->setExpire(3600);
        $this->assertEquals(3600, $this->session->getExpire());
    }
    
    /**
     * @expectedException \Vespula\Auth\Exception
     */
    public function testSetIdleException()
    {
        $this->expectException(\Vespula\Auth\Exception::class);
        $gc_max = ini_get('session.gc_maxlifetime');
        $new_max = $gc_max + 1;
        $this->session->setIdle($new_max);
        
    }
    
    /**
     * @expectedException \Vespula\Auth\Exception
     */
    /*
    public function testSetExpireException()
    {
        $this->expectException(\Vespula\Auth\Exception::class);

        $this->session->setExpire(999999999);
    
    }
    */
    public function testIsIdled()
    {
        $this->session->setIdle(1100);
        $this->assertTrue($this->session->isIdled());
        
        $this->session->setIdle(0);
        $this->assertFalse($this->session->isIdled());
    }
    
    public function testIsExpired()
    {
        $this->session->setExpire(1100);
        $this->assertTrue($this->session->isExpired());
        
        $this->session->setExpire(0);
        $this->assertFalse($this->session->isExpired());
    }
    
    /**
     * You can run with with php unit and --stderr otherwise, headers already sent
     */

    /*
    public function testRegenerateId()
    {
        $session_id = session_id();
        
        $this->session->regenerateId();
        
        $new_id = session_id();
        
        $this->assertTrue($session_id !== $new_id);
    }
    */
    
    public function testThirdConstructorParameter() {
        
        $session_obj = new Session(); // default args
        
        // check that data for $session_obj has been added to $_SESSION
        $this->assertTrue(
            array_key_exists('Vespula\\Auth\\Session\\Session', $_SESSION)
            && is_array($_SESSION['Vespula\\Auth\\Session\\Session'])
        );
        
        $session_obj2 = new Session(null, null, 'yabadabadoo-app'); // default args
        
        // check that data for $session_obj2 has also been added to $_SESSION
        $this->assertTrue(
            array_key_exists('yabadabadoo-app', $_SESSION)
            && is_array($_SESSION['yabadabadoo-app'])
        );
        
        // check that data for $session_obj is still in $_SESSION
        $this->assertTrue(
            array_key_exists('Vespula\\Auth\\Session\\Session', $_SESSION)
            && is_array($_SESSION['Vespula\\Auth\\Session\\Session'])
        );
    }

    public function testCallSetGet()
    {
        $session_obj = new Session(); // default args
        $session_obj->setFoo('Foo');

        $this->assertEquals("Foo", $session_obj->getFoo());

    }

    public function testCallSetDefault()
    {
        $session_obj = new Session(); // default args
        $actual = $session_obj->getXyz('Foo');

        $this->assertEquals("Foo", $actual);


        $null = $session_obj->getXyz();

        $this->assertNull($null);

    }

    /**
     * @expectedException \BadMethodCallException
     */
    public function testCallNotGetSet()
    {
        $this->expectException(BadMethodCallException::class);
        
        $this->session->foobar('xyz');
        
    }
}