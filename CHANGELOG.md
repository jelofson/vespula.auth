# Changelog

## Version 0.2.0

Updated the Text adapter to look at Apache-style .htpasswd files or an array of user data. 
This is a BC break over the 0.1.3 version. 

